import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Register() {

    const { user, setUser } = useContext(UserContext);

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [mobileNumber, setMobileNumber] = useState("");
    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        if (
            firstName.length > 0 &&
            lastName.length > 0 &&
            mobileNumber.length >= 11 &&
            email.length > 0 &&
            password1.length > 0 &&
            password2.length > 0 &&
            password1 === password2
        ) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [firstName, lastName, mobileNumber, email, password1, password2]);

    function registerUser(e) {
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName,
                lastName,
                mobileNumber,
                email,
                password1,
                password2
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            if (typeof data.access !== "undefined") {
                // localStorage.setItem('token', data.access);
                // retrieveUserDetails(data.access);
                
                Swal.fire({
                    title: "Registration Successful!",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })
            } else {
                Swal.fire({
                    title: "Duplicate email found",
                    icon: "error",
                    text: "Please provide a different email."
                })
            }
        });


        setUser({ email: email });
        setFirstName("");
        setLastName("");
        setMobileNumber("");
        setEmail("");
        setPassword1("");
        setPassword2("");
    };

    // const retrieveUserDetails = (token) => {
    //     fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
    //         headers: {
    //             Authorization: `Bearer ${token}`
    //         }
    //     })
    //     .then(res => res.json())
    //     .then(data => {
    //         console.log(data);
    //         setUser({
    //             id: data._id,
    //             isAdmin: data.isAdmin
    //         })
    //     })
    // };



    return (
           (user.id !== null)?
           <Navigate to ="/courses" />
           :
           <Form onSubmit={(e) => registerUser(e)}>
               <Form.Group controlId="firstName">
                   <Form.Label>First Name</Form.Label>
                   <Form.Control 
                       type="text" 
                       placeholder="Enter First Name" 
                       value={firstName}
                       onChange={e => setFirstName(e.target.value)}
                       required
                   />
               </Form.Group>

               <Form.Group controlId="lastName">
                   <Form.Label>Last Name</Form.Label>
                   <Form.Control 
                       type="text" 
                       placeholder="Enter Last Name" 
                       value={lastName}
                       onChange={e => setLastName(e.target.value)}
                       required
                   />
               </Form.Group>

               <Form.Group controlId="mobileNumber">
                   <Form.Label>Mobile Number</Form.Label>
                   <Form.Control 
                       type="tel" 
                       placeholder="Enter Mobile Number" 
                       value={mobileNumber}
                       onChange={e => setMobileNumber(e.target.value)}
                       minLength={11}
                       maxLength={11}
                       required
                   />
               </Form.Group>

               <Form.Group controlId="userEmail">
                   <Form.Label>Email address</Form.Label>
                   <Form.Control 
                       type="email" 
                       placeholder="Enter email" 
                       value={email}
                       onChange={e => setEmail(e.target.value)}
                       required
                   />
                   <Form.Text className="text-muted">
                       We'll never share your email with anyone else.
                   </Form.Text>
            </Form.Group>


            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password1}
                    onChange={e => setPassword1(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>password2</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
                    required
                />
            </Form.Group>

            { isActive ?
                <Button variant="primary" type="submit" id="submitBtn">
                Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }
        </Form>
    )

}





